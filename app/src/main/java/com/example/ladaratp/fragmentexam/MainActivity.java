package com.example.ladaratp.fragmentexam;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void selectFrag(View view) {
        Fragment fragment;
        if(view == findViewById(R.id.button2)) {
            fragment = new FragmentTwo();
        }else {
            fragment = new FragmentOne();
        }
        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment1 = fm.findFragmentById(R.id.fragment_place);
        if(fragment1 == null){
            fm.beginTransaction()
                    .add(R.id.fragment_place, fragment)
                    .commit();
        }else {
            fm.beginTransaction()
                    .replace(R.id.fragment_place, fragment)
                    .addToBackStack(null)
                    .commit();
        }
    }

}
