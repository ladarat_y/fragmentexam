package com.example.ladaratp.fragmentexam;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by ladaratp on 5/31/2016 AD.
 */
public class FragmentTwo extends ListFragment {
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(
                R.layout.fragment_two, container, false);

        String[] itemname ={
                "Safari",
                "Camera",
                "Global",
                "FireFox",
                "UC Browser",
                "Android Folder",
                "VLC Player",
                "Cold War"
        };


        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(), R.layout.chid_list_view, R.id.Itemname, itemname);

        setListAdapter(arrayAdapter);
        return rootView;
    }

    public void onListItemClick(ListView listView, View view, int posotion, long id){
        ViewGroup viewGroup = (ViewGroup) view;
        TextView textView = (TextView)viewGroup.findViewById(R.id.Itemname);
        Toast.makeText(getActivity(), textView.getText().toString(), Toast.LENGTH_LONG).show();
    }
}
